/**
 * Axios: promise-based HTTP client for Node.js and the browser
 * Cheerio: jQuery implementation for Node.js. Cheerio makes it easy to select, edit, and view DOM elements
 * Puppeteer: A Node.js library for controlling Google Chrome or Chronium.
 */

 /**
  * About: Data about the matches that is about to happen
  * Info: Live Matches or matches about to happen
  * Scraper Source: https://soccerstreams-100.com/
  */

 const axios = require('axios');
 const cheerio = require('cheerio');
 
 const streamsList = document.getElementById("streamsList");
 const url = 'https://soccerstreams-100.com/'
 const matches = []


 axios(url)
 .then(response => {
     const html = response.data;
     const $ = cheerio.load(html);
     const statsTable = $('.content');
     const table = statsTable.find('.post-content');
     const PremierLeaguematches = [];
     const LaLigaMatches = [];

     const logo = statsTable.find('');
     var links = [];

     table.each(function () {
         const league = $(this).find('.post-category').text().replace('\n','');
         const match = $(this).find('.post-title').text().replace('\n', '').replace('+', '').trim().split('vs');
         const link = $(this).find('.post-title').find('a').attr('href');
         const date = $(this).find('.post-date').text().replace('\n','');
         
         streamsList.push(link);
         if(league == 'Premier League') {
            PremierLeaguematches.push({
                Match: match[0] + "vs" + match[1],
                League: league,
                Link: link,
                Date: date,
                Urls: links,
            });
        }
         if(league == 'La Liga')
            LaLigaMatches.push({
                Match: match[0] + "vs" + match[1],
                League: league,
                Link: link,
                Date: date,
            });
         
     });

     getUrls(PremierLeaguematches[0].Link).then(value => links = value);
     console.log(links);
     console.log("Premier League");
     console.log(PremierLeaguematches);
     console.log("La Liga");
     console.log(LaLigaMatches);
 })
 .catch(console.error);

async function getUrls(url2) {
    let list = await axios(url2).then(response => {
        const html = response.data;
        const $ = cheerio.load(html);
        const container = $('div[class = "strm"] > table > tbody > tr');
        const LinkList = []

        container.each(function () {
        const link = $(this).find('a').attr('href');
            LinkList.push({
                Link: link,
            });
        });
        FinalLinkslist = [];
        FinalLinkslist.push(LinkList[0]);
        FinalLinkslist.push(LinkList[1]);
        FinalLinkslist.push(LinkList[2]);
        console.log(FinalLinkslist);
    })
    .catch(console.error);

    return list;
}